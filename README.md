# CTF Writeups

| CTF | Challenge | Tags | Difficulty |
|----|-----|----|----|
| Balsn CTF 2019 | Collision | crypto, crc, hash | hard |
| Hitcon CTF 2019 | Lost Modulus Again | crypto, rsa | medium |
| GreHack CTF 2019 | shady secrets sharing | crypto | easy |
| ASIS Final CTF 2019 | True zero | crypto, rev | hard |
