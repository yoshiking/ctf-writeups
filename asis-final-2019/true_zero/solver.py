from Crypto.Util.number import bytes_to_long, long_to_bytes
from Crypto.Util.strxor import strxor as xor
from scipy.optimize import brute
from binascii import unhexlify

def encrypt_fast(flag, flag_len, rounds_number):
    flagi = bytes_to_long(flag)
    for i in range(rounds_number):
        flagi = flagi ^ (flagi >> 1)
    one = (1 << (8 * flag_len - 1))
    return long_to_bytes(flagi | one)

def worker(data):
    known, byte, length, flag_stream, round_number = data
    payload = known + bytes([byte])
    flag_stream_prefix = encrypt_fast(payload, length + 1, round_number)[256:]
    if flag_stream[:len(flag_stream_prefix)] == flag_stream_prefix:
        return byte

def solve(rounds, condition, bytes_to_recover):
    offset = 0x329 # palette
    start = offset
    flag_enc = open('./flag.enc', 'rb').read()[start:]
    flag_len = start + len(flag_enc)
    keystream = "B2 70 90 79 15 96 AB 15 35 44 04 AB C9 8B E0 B3 D8 81 66 CB BD 6E 2E 86 36 24 4A F3 85 96 10 7B D8 01 21 5E 24 32 F7 2F".replace(" ", "")
    keystream = unhexlify(keystream.encode()) * 1000
    keystream = keystream[start - 256:]
    full_result = ""
    for round_number in rounds:
        print("[+] testing", round_number)
        zero_stream = encrypt_fast(b'\0' * flag_len, flag_len, round_number)[start:]
        flag_stream = xor(xor(keystream[:len(flag_enc)], flag_enc), zero_stream)
        flag_s = flag_stream
        known = b'\0' * offset
        working_len = 256
        length = len(known[start - working_len:])
        known = known[start - working_len:]
        for index in range(bytes_to_recover):
            results = []
            for i in range(0xff):
                results.append(worker((known, i, length, flag_s, round_number)))
            results = [k for k in results if k is not None]
            if len(results) > 1:
                print("[+] result:", results)
                break
            known = known + bytes([results[0]])
            known = known[1:]
            flag_s = flag_s[1:]
            full_result += chr(results[0])
            if condition(full_result):
                print("[*] match for", round_number)
                break

if __name__ == '__main__':
    solve(range(1, 1338), lambda x: "tRNS" in x, 64)
