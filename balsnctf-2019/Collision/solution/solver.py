from sage.all import *
from Crypto.Util.number import long_to_bytes, bytes_to_long
from Crypto.Cipher import AES, DES
import hashlib

p = int("""\
6816b2bba5ad70478c1beadb176b9ab17cb172841b10277f538f9d837f2\
2bdd807b970605c63859c739571cc535fd0c6879149b2d2eb676a182fd7\
5ff343e75a22ce75c36a775157c34f17\
""".replace(' ', ''), 16)


def undestory(x):
    assert x[-8:] == b'OAOOAOOO'
    x = x[8:-8]
    ks = []
    while len(ks) < 10:
        ks.append(hashlib.sha256(ks[-1] if ks else b'QAQQAQQQ').digest()[:8])
    for k in reversed(ks):
        des = DES.new(k, DES.MODE_CFB, k)
        x = des.decrypt(x)

    for _ in range(10):
        k = x[-16:]
        aes = AES.new(k, AES.MODE_CFB, k)
        x = k + aes.decrypt(x[:-16])

    x = bytes_to_long(x)
    x = int(pow(x, ZZ(Mod(31337, p-1)**-1), p))
    x = long_to_bytes(x)

    return x
