import gmpy2

prime = 8191

def pi(vals):
    accum = 1
    for v in vals:
        accum *= v
    return accum

def lagrange_interpolate(x, xs, ys, p):
    k = len(xs)
    assert k == len(set(xs)), "points must be distinct"
    nums = []
    dens = []
    for i in range(k):
        others = list(xs)
        cur = others.pop(i)
        nums.append(pi(x - o for o in others))
        dens.append(pi(cur - o for o in others))
    den = pi(dens)
    num = sum([ nums[i] * den * ys[i] % p * gmpy2.invert(dens[i], p) for i in range(k)])
    return (num * (gmpy2.invert(den, p) + p)) % p

nazo = ['KassKou', 'xXIPT4BL3SXx', 'cypherpunk', '__malloc_hook', 'Rainbow Bash', 'L1c0rn3P0w4', 'Patrick', 'k4l1_i5_b43', 'p0rt_f0rward3r', 'GH19{NotTheFlag}', 'Kabriolé', 'Kadabra']

with open('./secret_data_shhhhh.txt') as f:
    data = f.read()

datas = eval(data)

flag = ""
for i in range(len(datas)):
    shares = []
    for d in datas[i]:
        x = nazo.index(d[0]) + 1
        shares.append((x, d[1]))

    xs, ys = zip(*shares)
    flag += chr(lagrange_interpolate(0, xs, ys, prime))

print(flag)
